import { Hero } from './hero';

export interface HeroState {
    readonly heroes: Hero[];
    readonly hero: Hero;
}
