import { Action } from '@ngrx/store';
import * as types from './hero-action.types';
import { Hero } from 'src/app/models/hero';
 
export class loadHeroesAction implements Action {
  readonly type = types.LOAD_HEROES;
}
 
export class loadHeroesSuccessAction implements Action {
  readonly type = types.LOAD_HEROES_SUCCESS;
  constructor(public payload: Hero[]) {}
}

export class loadHeroAction implements Action {
  readonly type = types.LOAD_HERO;
  constructor(public payload: Hero) {}
}
 
export class loadHeroSuccessAction implements Action {
  readonly type = types.LOAD_HERO_SUCCESS;
  constructor(public payload: Hero) {}
}

export class createHeroAction implements Action {
  readonly type = types.CREATE_HERO;
  constructor(public payload: Hero) {}
}
 
export class createHeroSuccessAction implements Action {
  readonly type = types.CREATE_HERO_SUCCESS;
  constructor(public payload: Hero) {}
}

export class updateHeroAction implements Action {
  readonly type = types.UPDATE_HERO;
  constructor(public payload: Hero) {}
}
 
export class updateHeroSuccessAction implements Action {
  readonly type = types.UPDATE_HERO_SUCCESS;
  constructor(public payload: Hero) {}
}

export class deleteHeroAction implements Action {
  readonly type = types.DELETE_HERO;
  constructor(public payload: Hero) {}
}
 
export class deleteHeroSuccessAction implements Action {
  readonly type = types.DELETE_HERO_SUCCESS;
  constructor(public payload: Hero) {}
}
  
export type Actions = 
  loadHeroesAction | 
  loadHeroesSuccessAction | 
  loadHeroAction | 
  loadHeroSuccessAction |
  createHeroAction |
  createHeroSuccessAction |
  updateHeroAction |
  updateHeroSuccessAction |
  deleteHeroAction |
  deleteHeroSuccessAction
;