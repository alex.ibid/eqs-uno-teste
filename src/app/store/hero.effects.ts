import { Injectable } from "@angular/core";
import { HeroService } from "src/app/services/hero.service";
import { Effect, Actions, ofType } from '@ngrx/effects';
import { map, mergeMap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';
 
import * as types from './hero-action.types';
import * as heroActions from './hero.actions';
 
@Injectable({
  providedIn: 'root'
})
export class HeroEffects {
  constructor(
    private heroService: HeroService,
    private actions$: Actions
  ) {}
 
  @Effect() loadHeroes$: Observable<Action> = this.actions$.pipe(
    ofType<heroActions.loadHeroesAction>(types.LOAD_HEROES),
    mergeMap((res) => this.heroService.getHeroes().pipe(
      map(heroes => {
          return (new heroActions.loadHeroesSuccessAction(heroes))
        }
      )
    )),
  )

  @Effect() loadHero$: Observable<Action> = this.actions$.pipe(
    ofType<heroActions.loadHeroAction>(types.LOAD_HERO),
    mergeMap(res => this.heroService.getHero(res.payload).pipe(
      map(hero => {
          return (new heroActions.loadHeroSuccessAction(hero))
        }
      )
    )),
  )

  @Effect() createHero$: Observable<Action> = this.actions$.pipe(
    ofType<heroActions.createHeroAction>(types.CREATE_HERO),
    mergeMap(action => this.heroService.createHero(action.payload).pipe(
      map(hero => {
        return (new heroActions.createHeroSuccessAction(hero))
      })
    ))
  )

  @Effect() updateHero$: Observable<Action> = this.actions$.pipe(
    ofType<heroActions.updateHeroAction>(types.UPDATE_HERO),
    mergeMap(action => this.heroService.updateHero(action.payload).pipe(
      map(hero => {
        return (new heroActions.updateHeroSuccessAction(hero))
      })
    ))
  )

  @Effect() deleteHero$: Observable<Action> = this.actions$.pipe(
    ofType<heroActions.deleteHeroAction>(types.DELETE_HERO),
    mergeMap(action => this.heroService.deleteHero(action.payload).pipe(
      map(hero => {
        return (new heroActions.deleteHeroSuccessAction(hero))
      })
    ))
  )
 
}