import * as heroActions from './hero.actions';
import * as types from './hero-action.types';
import { HeroState } from 'src/app/models/hero-state';
 
export const initialState: HeroState = {
  heroes: [],
  hero: <any>{}
}
 
export function HeroReducer(state = initialState, action: heroActions.Actions):HeroState {
  switch(action.type) {
    case types.LOAD_HEROES_SUCCESS: {
      return {... state, heroes: action.payload };
    }
    case types.LOAD_HERO_SUCCESS: {
      return {... state, hero: action.payload };
    }
    case types.CREATE_HERO_SUCCESS: {
      return {... state, hero: action.payload };
    }
    case types.UPDATE_HERO_SUCCESS: {
      return {... state, hero: action.payload };
    }
    case types.DELETE_HERO_SUCCESS: {
      return {...state, heroes: state.heroes.filter(hero => hero.id !== action.payload.id)}
    }
    default:
      return state;
  }
}