import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

import { environment as env } from 'src/environments/environment';
import { Hero } from 'src/app/models/hero';
import { Observable, throwError } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

import { HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};


@Injectable({
  providedIn: 'root'
})
export class HeroService {

  constructor( private http: HttpClient) { }

  public getHeroes(): Observable<Hero[]> {
    return this.http
      .get<Hero[]>(env.apiurl + 'heroes', httpOptions )
      .pipe(
        tap(response => {
          return response;
        }),
        catchError(this.handleError)
      );
  }

  public getHero(payload: Hero): Observable<Hero> {
    return this.http
      .get<Hero>(env.apiurl + 'heroes/'  + payload, httpOptions )
      .pipe(
        tap(response => {
          return response;
        }),
        catchError(this.handleError)
      );
  }

  public createHero(payload: Hero): Observable<Hero> {
    return this.http
      .post<Hero>(env.apiurl + 'heroes', payload, httpOptions)
      .pipe(
        tap(response => {
          console.log('Create',`hero ${payload.name}`)
          return response;
        }),
        catchError(this.handleError)
      )
  }

  public updateHero(payload: Hero): Observable<Hero> {
    return this.http
      .patch<Hero>(env.apiurl + 'heroes/' + payload.id, payload, httpOptions)
      .pipe(
        tap(response => {
          console.log('Update',`hero ${payload.name}`)
          return response;
        }),
        catchError(this.handleError)
      )
  }

  public deleteHero(payload: Hero): Observable<Hero> {
    return this.http.delete<Hero>(env.apiurl + 'heroes/' + payload.id, httpOptions)
      .pipe(
        map(() => {
          console.log('Delete',`hero ${payload.name}`)
          return payload;
        }),
        catchError(this.handleError)
      );
  }

  private handleError(responseerror: Response | any): Observable<any>  {
    return throwError(responseerror.message);
  }
}
