import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeroEditorComponent } from './hero-editor/hero-editor.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'hero-update/:id',
    component: HeroEditorComponent
  },
  {
    path: 'hero-create',
    component: HeroEditorComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
