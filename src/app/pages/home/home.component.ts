import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  listing = 'cards';
  title = 'uno-eqs';
  constructor(
    public router: Router
  ) { 
    
  }

  ngOnInit() {
  }

  toogleList(list) {
    this.listing = list;
  }

}
