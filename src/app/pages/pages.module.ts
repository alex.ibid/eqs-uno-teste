import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReactiveFormsModule } from '@angular/forms';

import { PagesRoutingModule } from './pages-routing.module';
import { HeroListComponent } from './hero-list/hero-list.component';
import { HeroEditorComponent } from './hero-editor/hero-editor.component';
import { HomeComponent } from './home/home.component';



@NgModule({
  declarations: [
    HeroListComponent,
    HeroEditorComponent,
    HomeComponent
  ],
  imports: [
    CommonModule,
    PagesRoutingModule,
    ReactiveFormsModule,
  ],
  providers: [],
})
export class PagesModule { }
