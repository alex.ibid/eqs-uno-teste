import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HeroService } from 'src/app/services/hero.service';
import { Store } from '@ngrx/store';
import { HeroState } from 'src/app/models/hero-state';
import { Hero } from 'src/app/models/hero';
import { Observable } from 'rxjs';

import * as HeroActions from 'src/app/store/hero.actions';

@Component({
  selector: 'app-hero-editor',
  templateUrl: './hero-editor.component.html',
  styleUrls: ['./hero-editor.component.scss']
})
export class HeroEditorComponent implements OnInit {

  hero$: Observable<any>;
  heroId;

  HeroForm: FormGroup;
  uploadimage = '/assets/salesforce-lightning-design/images/profile_avatar_200.png';  

  constructor(
    private store: Store<HeroState>,
    private fb: FormBuilder,
    private db: HeroService,
    public router: Router,
    private route: ActivatedRoute,
  ) { 
    this.heroForm();
  }

  ngOnInit() {

    if(this.router.url === '/hero-create') {
      this.HeroForm.reset();
    } else {
      this.hero$ = this.store.select('applicationState');
      this.route.params.subscribe((params: Hero) => {
        this.heroId = params.id;
        this.getHero(params.id);
      });
    }
    
  }

  getHero(id) {
    this.store.dispatch(new HeroActions.loadHeroAction(id));
    this.hero$.subscribe((state:HeroState) => {
      if(Object.keys(state.hero).length !== 0){
        this.uploadimage = 'assets/heroes/' + state.hero.image;
        this.HeroForm.setValue({
          name: state.hero.name,
          email: state.hero.email,
          phone: state.hero.phone,
          image: state.hero.image
        });
      }
    });
  }

  heroForm() {
    this.HeroForm = this.fb.group({
      'name': ['', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(30),
      ]],
      'email': ['', [
        Validators.required,
        Validators.email
      ]],
      'phone': ['', [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(20),
        Validators.pattern('[0-9\+\(\) ]+')
      ]],
      'image': ['', [
        Validators.required
      ]],
    });
  }

  get name(){
    return this.HeroForm.controls.name;
  }
  get email(){
    return this.HeroForm.controls.email;
  }
  get phone(){
    return this.HeroForm.controls.phone;
  }
  get image(){
    return this.HeroForm.controls.image;
  }

  onHeroUpload(event) {
    this.uploadimage = '/assets/heroes/' + event.target.files[0].name;
    this.image.setValue(event.target.files[0].name)
  }

  submitHero(){
    let payload = this.HeroForm.value;

    if(this.router.url === '/hero-create') {
      this.store.dispatch(new HeroActions.createHeroAction(payload));
      this.router.navigate(['/']);
    } else {
      payload.id = this.heroId;
      this.store.dispatch(new HeroActions.updateHeroAction(payload));
      this.router.navigate(['/']);
    }
  }

}
