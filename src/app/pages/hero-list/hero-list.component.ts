import { Component, OnInit } from '@angular/core';
import { Hero } from 'src/app/models/hero';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { HeroState } from 'src/app/models/hero-state';
import * as HeroActions from 'src/app/store/hero.actions';

@Component({
  selector: 'app-hero-list',
  templateUrl: './hero-list.component.html',
  styleUrls: ['./hero-list.component.scss']
})
export class HeroListComponent implements OnInit {

  listing = 'cards';

  heroes$: Observable<any>;
  heroes: Hero[];

  constructor(
    private store: Store<HeroState>
  ) { 
    this.heroes$ = this.store.select('applicationState');
  }

  ngOnInit() {
    this.getHeroes();
  }

  toogleList(list) {
    this.listing = list;
  }

  getHeroes() {
    this.store.dispatch(new HeroActions.loadHeroesAction);
    this.heroes$.subscribe((state:HeroState) => this.heroes = state.heroes);
  }

  deleteHero(hero: Hero): void {
    this.store.dispatch(new HeroActions.deleteHeroAction(hero));
    this.getHeroes();
  }


}
